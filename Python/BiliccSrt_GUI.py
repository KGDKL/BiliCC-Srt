import tkinter,threading,BiliccSrt_Down          #载入模块
import browser_cookie3


# 修订日期 20240122


# BV13f4y1G7sA 无压缩
# BV18a4y1H73s 很多P


def xiaZai():
    if (BiliccSrt_Down.cookie == BiliccSrt_Down.cookieDe ):
        print ('【错误】 Cookie未加载，请在浏览器中登录B站、再点击对应浏览器的按钮加载Cookie。')
        return
    if (avhao.get() == '' ):
        print ('【错误】 BV号未输入，请输入BV号，如BV13f4y1G7sA。')
        return
    xiancheng1 = threading.Thread(target=zzz)   # 定义线程，运行抓取程序
    xiancheng1.start()                          # 让线程开始工作
def zzz():
    BiliccSrt_Down.downAll(avhao.get())

def getFxCo():
    BiliccSrt_Down.cookie = browser_cookie3.firefox()
    print ('已加载火狐Cookie。')


# 单选按钮回调函数,就是当单选按钮被点击会执行该函数
def radCall():
    radSel = radVar.get()
    if radSel == 1:
        BiliccSrt_Down.bianma = 'utf-8'
        print ('编码输出变更为：UTF-8')
    elif radSel == 2:
        BiliccSrt_Down.bianma = 'utf-16'
        print ('编码输出变更为：UTF-16')


### Tk GUI窗口 ###
chaungkou = tkinter.Tk()
chaungkou.title('BLIBILI CC字幕下载器 V5')
chaungkou.geometry('400x250')

diyijie = tkinter.Frame(chaungkou)      #第一节
dierjie = tkinter.Frame(chaungkou)      #第二节
disanjie = tkinter.Frame(chaungkou)      #第三节
disijie = tkinter.Frame(chaungkou)      #第四节

biaoqian = tkinter.Label(chaungkou,text='BLIBILI CC字幕下载器 V5',width=20,height=2,font=("微软雅黑",22))   #标签
biaoqian.pack()

diyijie.pack()  #显示一二三节
dierjie.pack()
disijie.pack()
disanjie.pack()


tkinter.Label(diyijie,text='目标BV号：',width=8,height=1,font=("微软雅黑",15)).pack(side='left')      #标签
avhao = tkinter.Entry(diyijie)                                                                      #BV号输入框
avhao.pack(side='right')

radVar = tkinter.IntVar()    # 通过tk.IntVar() 获取单选按钮value参数对应的值
rad1 = tkinter.Radiobutton(dierjie,text='UTF-8     ',variable=radVar,value=1,command=radCall,font=("微软雅黑",15))        #单选框
rad1.pack(side='left')
rad2 = tkinter.Radiobutton(dierjie,text='UTF-16',variable=radVar,value=2,command=radCall,font=("微软雅黑",15))
rad2.pack(side='right')


anniu1 = tkinter.Button(disanjie,text='抓取',command=xiaZai,width=15,height=1,font=("微软雅黑",18))        #按钮
anniu1.pack(side='right')


buFx = tkinter.Button(disijie,text='加载火狐Cookie',command=getFxCo,width=15,height=1,font=("微软雅黑",18))        #按钮
buFx.pack(side='right')




print ('''
    ### 1、在浏览器中登录B站、再点击对应浏览器的按钮加载Cookie，目前只支持火狐浏览器。
    ### 2、输入BV号，如BV13f4y1G7sA，点击“抓取”按钮，等待下载完成即可。
    ### 编码默认输出UTF-8，如果某些播放器下字幕乱码，可尝试选择UTF-16编码。
    ### 供测试的BV号示例：BV13f4y1G7sA、BV1XA411G7ib、BV18a4y1H73s(这个非常多，慎用)。
''')

chaungkou.mainloop()